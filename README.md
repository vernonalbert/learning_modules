# Machine Learning and Image Processing Tutorials and Examples

## This repository contains code examples for Machine Learning and Computer Vision 

1) Computer Vision Projects
   1) Contains Practice Assessments on -:
   
      a) Numpy and Image Basics
   
      b) Image Basics with OpenCV
   
      c) Image Processing
   
      d) Video Basics with Python and OpenCV
   
      e) Object Detection
   
      f) Deep learning for Computer Vision
   
    2) Template Matching
   
2) Machine Learning Projects

   Contains examples on -:
   
   a) Deep Learning for Custom Images 
   
   b) Linear Gradient Descent
   
   c) Regression Fitting
   
   
3) SQL Project

   Practical example on data manupulation of Multiple tables

4) Docker_ML_APP
   
   Simple example of deployment of a linear regression app to predict salary based on experience. App created with Flask and HTML and Deployment with docker.
   Salary_Data.csv from https://www.kaggle.com/vihansp/salary-data

For this course, I used python3.6

Required dependancies,external files and Annanconda Prompt Installation codes are as as follows -:

1) Open CV

   pip install opencv-python
   
2) Keras

   pip install Keras
   
3) scikit - learn

   pip install scikit-learn
   
4) Tensor Flow

   pip install --upgrade tensorflow
   
5) Data for Deep-Learning-Custom-Images.ipynb

   https://drive.google.com/file/d/1U6RtBhML-Lj0w2suve0UhQmwcF5pfJCm/view
 
